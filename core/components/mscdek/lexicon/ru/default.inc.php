<?php
include_once 'setting.inc.php';

$_lang['mscdek'] = 'msRussianPost';
$_lang['mscdek_time'] = 'Срок доставки в [[+city]]: [[+time]] дней';
$_lang['mscdek_time_na'] = 'неопределенное количество';
$_lang['mscdek_time_nf'] = 'Извините, выбранный метод  недоступен для доставки в [[+city]]';

$_lang['mscdek_frontend_city_select'] = 'Выберите город';

$_lang['mscdek_err_type'] = 'Указан некорректный тип доставки: [[+type]]';