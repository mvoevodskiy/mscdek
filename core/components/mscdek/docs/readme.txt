--------------------
msCDEK
--------------------
Author: Mikhail S. Voevodskiy <mv@compaero.ru>
--------------------

An for calculate tariffs of CDEK for minishop2 - eCommerce solution for MODx Revolution.

Feel free to suggest ideas/improvements/bugs on BitBucket:
https://bitbucket.org/mvoevodskiy/mscdek/issues