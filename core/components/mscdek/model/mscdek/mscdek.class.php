<?php

/**
 * The base class for msCDEK.
 */
class msCDEK {
	/* @var modX $modx */
	public $modx;
    /** @var minishop2 ms2 */
    public $ms2;
    /** @var CalculatePriceDeliveryCdek $cdek */
    public $cdek = null;
    public $deliveryHandler = 'msCDEKHandler';
    public $cityNames = array();
    public $cityNamesLower = array();
    public $cityIds = array();
    public $countryNames = array();
    public $countryNamesLower = array();
    public $country = '';
    public $countryDefault = 'RUS';
    public $initialized = false;


    /**
	 * @param modX $modx
	 * @param array $config
	 */
	function __construct(modX &$modx, array $config = array()) {
		$this->modx =& $modx;

		$corePath = $this->modx->getOption('mscdek_core_path', $config, $this->modx->getOption('core_path') . 'components/mscdek/');
		$assetsUrl = $this->modx->getOption('mscdek_assets_url', $config, $this->modx->getOption('assets_url') . 'components/mscdek/');
		$connectorUrl = $assetsUrl . 'connector.php';

		$this->config = array_merge(array(
			'assetsUrl' => $assetsUrl,
			'cssUrl' => $assetsUrl . 'css/',
			'jsUrl' => $assetsUrl . 'js/',
			'imagesUrl' => $assetsUrl . 'images/',
			'connectorUrl' => $connectorUrl,

			'corePath' => $corePath,
			'modelPath' => $corePath . 'model/',
			'chunksPath' => $corePath . 'elements/chunks/',
			'templatesPath' => $corePath . 'elements/templates/',
			'chunkSuffix' => '.chunk.tpl',
			'snippetsPath' => $corePath . 'elements/snippets/',
			'processorsPath' => $corePath . 'processors/',

            'name_prefix' => $this->modx->getOption('mscdek_name_prefix'),
            'weight_in_kg' => $this->modx->getOption('ms2_delivery_weight_in_kg'),
            'default_size' => $this->modx->getOption('mscdek_default_size', null, 1),
            'cities_list' => $corePath . $this->modx->getOption('mscdek_cities_list'),
            'countries_list' => $corePath . $this->modx->getOption('mscdek_countries_list'),
            'login' => $this->modx->getOption('mscdek_login', null, ''),
            'password' => $this->modx->getOption('mscdek_password', null, ''),
            'isAjax' => !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest',
            'returnTime' => $this->modx->getOption('mscdek_return_time', null, true),

		), $config);

		$this->modx->addPackage('mscdek', $this->config['modelPath']);
		$this->modx->lexicon->load('mscdek:default');

        $this->initialize();

    }

    public function initialize() {
        if (!$this->initialized) {
            $this->ms2 = $this->modx->getService('minishop2');
            if (!$this->ms2->initialized) {
                $this->ms2->initialize($this->modx->context->get('key'), array('json_response' => true));
            }

            $this->initCountries();
            $this->initCities();


            $orderData = $this->ms2->order->get();
            $countries = array_flip($this->countryNamesLower);
//
            if (isset($orderData['country']) and $orderData['country'] != '') {
                $this->country = strtoupper($countries[strtolower($orderData['country'])]);
            } else {
                $this->country = $this->countryDefault;
            }
//            $this->modx->log(1, '$orderData: '.print_r($_POST,1));
//            $this->modx->log(1, 'this->country: '.$this->country);
//            $this->modx->log(1, '$orderData: '.print_r($orderData,1));

            if (isset($orderData['city']) and !empty($orderData['city']) and !in_array(strtolower($orderData['city']), $this->cityNamesLower[$this->country])) {
//                $this->modx->log(1,'incorrect country <--> city');
                $orderData['city'] = '';
                $this->ms2->order->set($orderData);
            }

            if ($js = trim($this->modx->getOption('mscdek_frontend_js'))) {
                if (!empty($js) && preg_match('/\.js/i', $js)) {
//                $this->modx->regClientScript(preg_replace(array('/^\n/', '/\t{7}/'), '', '
//							<script type="text/javascript">
//								if(typeof jQuery == "undefined") {
//									document.write("<script src=\"'.$this->config['jsUrl'].'web/lib/jquery.min.js\" type=\"text/javascript\"><\/script>");
//								}
//							</script>
//							'), true);
                    $this->modx->regClientScript(str_replace('[[+jsUrl]]', $this->config['jsUrl'], $js));
                }
            }
            $this->initialized = true;
        }
    }

    public function deliveryOwner() {

        $orderData = $this->ms2->order->get();
        if ($delivery = $this->modx->getObject('msDelivery', $orderData['delivery'])) {
            if ($delivery->get('class') == $this->deliveryHandler) {
                if ($delivery->get('country')) $this->country = $delivery->get('country');
                return true;
            }
        }

        return false;
    }



    public function getCost(msOrderInterface $order, msDelivery $delivery, $cart_cost = 0) {

        if ($delivery->get('class') !== $this->deliveryHandler) return '';

        $cart = $this->ms2->cart->status();
        $orderData = $order->get();

        $sendingData = $this->getSendingData($this->getTariffId($delivery), $cart['total_weight'], $orderData['city']);
        if (is_array($sendingData)) $cart_cost += $sendingData['cost'];
        return $cart_cost;
    }


    public function getTime()
    {

        $orderData = $this->ms2->order->get();
        $result = '';
        /** @var msDelivery $delivery */
        if ($orderData['city'] and $delivery = $this->modx->getObject('msDelivery', $orderData['delivery'])) {
            if ($delivery->get('class') !== $this->deliveryHandler) {
                return false;
            }
            $cart = $this->ms2->cart->status();
            $sendingData = $this->getSendingData($this->getTariffId($delivery), $cart['total_weight'],
                $orderData['city']);

            if ($sendingData['deliveryPeriodMin'] != -1) {
                if ($this->config['returnTime']) {
                    $time = $sendingData['deliveryPeriodMin'] == $sendingData['deliveryPeriodMax']
                        ? $sendingData['deliveryPeriodMin']
                        : $sendingData['deliveryPeriodMin'] . ' - ' . $sendingData['deliveryPeriodMax'];
                    $result = $this->modx->lexicon(
                        'mscdek_time',
                        array('time' => $time, 'city' => $orderData['city'])
                    );
                }
            } else {
                $result = $this->modx->lexicon('mscdek_time_nf', array('city' => $orderData['city']));
            }
        } else {

        }
        return $result;
    }


    /**
     * @param msDelivery $delivery
     * @return string
     */
    public function getTariffId($delivery) {

        $props = $delivery->get('properties');

        if (isset($props['tariffId'])) return $props['tariffId'];
        else return 0;

    }


    public function getSendingData($tariffId, $weight, $to) {

        $error = null;
        $errorFields = array();
        $size = $this->config['default_size'];
        $result = array();
//        $tariffId = 137;

        if (!$this->cdek) $this->getCdek();
        if (!$this->config['weight_in_kg']) $weight = $weight / 1000;

        $tariffId = (int) $tariffId;
        if ($tariffId) $this->cdek->setTariffId($tariffId);
        else $errorFields['tariff'] = array('orig' => $tariffId, 'real' => $tariffId);

        $cityFrom = $this->modx->getOption('mscdek_from_cityid');
        $cityFromId = (int) $cityFrom;
        if ($cityFromId) $this->cdek->setSenderCityId($cityFromId);
        else $errorFields['from'] = array('orig' => $cityFrom, 'real' => $cityFromId);

        $cityToId = (int) $this->getCityId($to);
        if ($cityToId) $this->cdek->setReceiverCityId($cityToId);
        else $errorFields['to'] = array('orig' => $to, 'real' => $cityToId);

        if ($this->config['login'] and $this->config['password']) $this->cdek->setAuth($this->config['login'], $this->config['password']);

//        $this->cdek->addGoodsItemByVolume($weight, $this->config['default_size']);
        $this->cdek->addGoodsItemBySize($weight,$size,$size,$size);


//        $this->modx->log(1, 'to: '. $to);
//        $this->modx->log(1, 'to: '. ((int) $this->getCityId($to)));
//        $this->modx->log(1, 'tariffId: '. ((int) $tariffId));
//        $this->modx->log(1, 'senderCity: '. ((int) $this->modx->getOption('mscdek_from_cityid')));
//        $this->modx->log(1, 'weight: '. $weight);
//        $this->modx->log(1, 'size: '. $this->config['default_size']);

        if (empty($errorFields)) {
            if ($this->cdek->calculate()) {
                $delivery = $this->cdek->getResult();

//            $this->modx->log(1, print_r($delivery,1));

                $result = $delivery['result'];
                $result['cost'] = $result['price'];
                if ($result['deliveryPeriodMin'] == $result['deliveryPeriodMax']) $result['time'] = $result['deliveryPeriodMin'];
                else $result['time'] = $result['deliveryPeriodMin'] . ' - ' . $result['deliveryPeriodMax'];
                unset($result['price']);
            } else {
                $error = $this->cdek->getError();
                $this->modx->log(xPDO::LOG_LEVEL_ERROR, 'Произошла ошибка при получении данных от СДЕК: '. print_r($error['error'][0]['text'],1));
                $result = array('error' => 'Извините, доставка не может быть рассчитана. Код ошибки: '.$error['error'][0]['code']);
            }
        } else {
            foreach ($errorFields as $k => $v) {
                $this->modx->log(xPDO::LOG_LEVEL_WARN, 'Доставка СДЕК. Неправильно указано поле '. $k .': '.$v['orig'].' ('. $v['real'] .')');
                $result['error_'.$k] = 'Неправильно указано поле '. $k .': '.$v['orig'].' ('. $v['real'] .')';
            }
            $result['error'] = 'Извините, доставка не может быть рассчитана.';
        }


        return $result;

    }

    public function getCities(/*$limit = 0*/) {

        if ($this->country) {
            return $this->cityNames[$this->country];
        } else return array();

    }

    public function getCityId($cityName) {

            return array_search(strtolower($cityName), $this->cityNamesLower[$this->country]);

    }

    public function getAreas($type = 'cities') {
        switch ($type) {
            case 'country': return $this->countryNames;
            case 'city': return $this->cityNames[$this->country];
            default: return array();
        }
    }

    public function initCountries() {
        if ($countries = file_get_contents($this->config['countries_list'])) {
            $this->countryNames = unserialize($countries);
            foreach($this->countryNames as $id => $name) {
                $this->countryNamesLower[$id] = strtolower($name);
            }
            return true;
        } else return false;
    }

    public function initCities() {

        if ($cities = file_get_contents($this->config['cities_list'])) {

            $this->cityNames = unserialize($cities);
//            exit('<pre>'.print_r($this->cityNames,1));
            foreach ($this->cityNames as $country => $cities) {
                $countryCities = array();
                foreach ($cities as $id => $name) {
                    $countryCities[$id] = strtolower($name);
                }
                $this->cityNamesLower[$country] = $countryCities;
//            $this->modx->log(1,print_r($this->cityNamesLower,1));

//            $this->cityNamesLower = array_walk(&$this->cityNames, 'strtolower');
                //$this->cityIds = array_flip($this->cityNames);
            }
            return true;

        } else return false;

    }

    /**
     * @return null
     */
    public function getCdek() {

        if (!$this->cdek) $this->cdek = $this->modx->getService('calculatepricedeliverycdek', 'CalculatePriceDeliveryCdek', $this->config['corePath'].'libs/');
        return $this->cdek;

    }

}