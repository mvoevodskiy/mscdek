<?php
/** @var array $scriptProperties */
/** @var msCDEK $msCDEK */
if (!$msCDEK = $modx->getService('mscdek', 'msCDEK', $modx->getOption('mscdek_core_path', null, $modx->getOption('core_path') . 'components/mscdek/') . 'model/mscdek/', $scriptProperties)) {
    return 'Could not load msCDEK class!';
}

// Do your snippet code here. This demo grabs 5 items from our custom table.
$tpl = $modx->getOption('tpl', $scriptProperties, 'option');
$type = $modx->getOption('type', $scriptProperties, 'city');
$sortby = $modx->getOption('sortby', $scriptProperties, 'name');
$sortdir = $modx->getOption('sortdir', $scriptProperties, 'ASC');
$limit = $modx->getOption('limit', $scriptProperties, 5);
$outputSeparator = $modx->getOption('outputSeparator', $scriptProperties, "\n");
$toPlaceholder = $modx->getOption('toPlaceholder', $scriptProperties, false);

$output = '';
$areas = array();
$areasRaw = $msCDEK->getAreas($type);

//$modx->log(1, '>> areas: '. print_r($areasRaw,1));
$areas = array_values($areasRaw);
sort($areas);
foreach ($areas as $area) {
    $output .= $modx->getChunk($tpl,array('area' => $area));
}

if (!empty($toPlaceholder)) {
    // If using a placeholder, output nothing and set output to specified placeholder
    $modx->setPlaceholder($toPlaceholder, $output);

    return '';
}
// By default just return output
//$modx->log(1, $output);
return $output;
